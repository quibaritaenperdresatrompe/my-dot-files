DEFAULT_USER=valentin

# Path to your oh-my-zsh installation.
  export ZSH=/home/valentin/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

PROJECTS=~/Bureau/workspace

plugins=(git)

# LOAD SHELL ALIASES

. ~/.aliases/git.sh
. ~/.aliases/mongo.sh
. ~/.aliases/node.sh
. ~/.aliases/zsh.sh

source $ZSH/oh-my-zsh.sh

cd $PROJECTS

# Launch tmux
if [[ ! $TERM =~ screen ]]; then
  exec tmux
fi
